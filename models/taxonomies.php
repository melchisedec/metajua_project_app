<?php

require_once( 'Medoo.php' );


class taxonomies{

    private $table = "taxonomies";
    public $db; 

    public function __construct( $db_configs ){
        $this->db = new Medoo( $db_configs );

    }

    //adding 2 rec

    public function create($singular, $plural, $remote_id = null){        
           $query = $this->db->insert($this->table, [
                        "singular" => $singular,
                        "plural" => $plural
                    ]);

           return $this->db->id()> 0 ? true:false;

    }

    //adding 2 rec
    public function read($columns = []){
        if($columns != []){

                    $records = $this->db->select("taxonomies", [$columns], [
                    ]);
        }
        else {
            $records = $this->db->select("taxonomies", "*");
        }
        return isset($records) ? json_encode($records) : [];

    }

    //updating record
    public function update($update_id,$column,$new_value){
        $id = (int) $update_id;

        $update = $this->db->update("taxonomies", [
            $column => $new_value
         ], [
            "id" => $update_id
        ]);

        return $update > 0 ? $update : 0;
    }

    // Delete record
    public function delete($id){
        $delete_id = (int) $id;

        $delete = $this->db->delete("taxonomies", [ 
            "id[<]" => $delete_id 
        ]);
        return $delete > 0 ? true:false;
    }

}


$taxonomies = new taxonomies( [
	'database_type' => 'mysql',
	'database_name' => 'mydb',
	'server' => 'localhost',
	'username' => 'metajua',
	'password' => 'metajua',
	'charset' => 'utf8'
] );
//post_meta and users;

?>