<?php
	require_once('Medoo.php');

	class Users{
		private $table = "users";
		public $db;

		public function __construct( $db_configs ){
			$this->db = new Medoo($db_configs);
		}

		//create === adding a record
		public function create( $login,$password,$remote_id = null ){

			//$record = ["login","password"];

			$create = $this->db->insert($this->table, [
				"login" => $login,
				"password" => $password
			]);
			return $this->db->id() > 0 ?  true: false;
		}

		//read === display records


		public function read( $columns = [] ){
			if( $columns != [] ){
				$records = $this->db->select($this->table, [$columns], []);
			}else{
				$records = $this->db->select($this->table, "*");
			}
			return $records;
		}

		public function update( $id = null,$login_new_value,$password_new_value ){
			$edit_id = (int) $id;
			$update = $this->db->update($this->table, [
				"login" => $login_new_value, 
				"password" => $password_new_value
				], ["id" => $edit_id
			]);
			return $update > 0 ? $update : 0;
		}

		public function delete( $id ){
			$delete_id = (int) $id;			
			$delete = $this->db->delete($this->table, [
					"AND" => ["id" => $id]
			]);
			return $delete;
		}


	}

	$app_user = new Users([
		"database_type" => "mysql",
		"database_name" => "mydb",
		"server" => "localhost",
		"username" => "metajua",
		"password" => "metajua",
		"charset" => "utf8"
	]);